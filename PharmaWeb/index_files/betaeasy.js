/**
 * Getting current Browser locale
 * @author  Viodele <viodele@gmail.com>
 */
var BrowserLocale = {
    /**
     * Default page locale
     * @param   {String}  defaultLocale
     * @author  Viodele <viodele@gmail.com>
     */
    defaultLocale:  'en',
    /**
     * Array of available locales
     * @param   {Array}   available   Array of available locales
     * @author  Viodele <viodele@gmail.com>
     */
    available:      new Array('en', 'ca', 'pl', 'fr', 'uk', 'de', 'ru', 'af', 'fi', 'nl', 'el', 'it', 'ja', 'cs'),
    /**
     * Detecting locale from the browser defaults
     * @return  {String}
     * @author  Viodele <viodele@gmail.com>
     */
    detect:         function() {
        if(navigator) {
            if(navigator.language) {
                return navigator.language;
            }
            else if(navigator.browserLanguage) {
                return navigator.browserLanguage;
            }
            else if(navigator.systemLanguage) {
                return navigator.systemLanguage;
            }
            else if(navigator.userLanguage) {
                return navigator.userLanguage;
            }
        }
        return this.defaultLocale;
    },
    /**
     * Checking locale name
     * @param   {String}    locale
     * @return  String
     * @author  Viodele <viodele@gmail.com>
     */
    check:          function(locale) {
        locale = locale.toLowerCase();
        var _locale = '';
        var __locale = '';
        for(i in this.available) {
            _locale = this.available[i];
            if(locale == _locale) {
                return locale;
            }
        }
        if(!locale.match(/^[a-z]{2}$/)) {
            /*
             * Compensating detected locale
             */
            var locales = locale.match(/[a-z]{2}/g);
            for(i in locales) {
                _locale = locales[i];
                for(j in this.available) {
                    __locale = this.available[j];
                    if(_locale == __locale) {
                        return __locale;
                    }
                }
            }
        }
        return 'en';
    },
    /**
     * Getting available locale for current browser
     * @return  String
     * @author  Viodele <viodele@gmail.com>
     */
    init:           function() {
        var locale = this.detect();
        locale = this.check(locale);
        return  locale;
    }
}

var LocaleDir = '';//'locales/' + BrowserLocale.init() + '/';

if(!BetaEasy) {
    function tplCompile(tpl, params) {
        return tpl.replace(/\{\$([^}]+)}/g,
            function(fullMatchNotUsed, match) {
                return params[match];
            }
        );
    }

    BETAEASY_LINK = 'http://app.betaeasy.com/';

    //class BetaEasy
    var BetaEasy = {
        //private properties
        config: {
          betaId: null,
          buttonAlign: 'right',
          buttonBackgroundColor: '#f00',
          buttonTextColor: '#fff',
          buttonHeight: 137,
          // Old - two images on button(active and hover), New - css sprite;
          // TODO: When new style will be accepted - remove this
          styleType: "old",
          buttonMouseHoverBackgroundColor: '#00f',
          buttonImageActive: 'active-1.png',
          buttonImageHover: 'hover-1.png',
          buttonText: 'Feedback',
          popupHeight: 610,
          popupWidth: 490,
          language: 'en'
        },

        cssPopupStyles: "#BetaEasyContributionForm{z-index: 1000002; margin: 0 auto; position: absolute; display: none;} #BetaEasyPageCover {position: absolute; opacity: .6; filter: alpha(opacity=60); z-index: 1000001; background-color: #000; left: 0; top: 0; width: 100%; height: 100%;}",
        cssButtonStyles: "#BetaEasyInvokeButton a {color:white; font-size:smaller; font-weight:bold; text-align:center; text-decoration:none; width: {$buttonWidth}px; height: {$buttonHeight}px; background: url({$cdnurl}css/themes/badges/{$feedButtonActive}) -35px 0px no-repeat; z-index: 1000000;}\r\n* #BetaEasyInvokeButton a:hover {width: {$buttonWidth}px; height: {$buttonHeight}px; background: url({$cdnurl}css/themes/badges/{$feedButtonHover}) -35px -137px no-repeat; z-index: 1000000;}\r\n* html #BeataEasyInvokeButtonBody {/*\*/position: absolute; top: expression(({$buttonTop} + (superMegaHelper = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)) + 'px'); right: expression(({$buttonRight} + (superMegaHelper2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft)) + 'px');/**/ }\r\n#BetaEasyInvokeButton > #BeataEasyInvokeButtonBody {position: fixed; top: {$buttonTop}px; right: {$buttonRight}px;}",

        //public methods
        init: function(config) {
            for(var i in config) {
                this.config[i] = config[i];
            }

            //window.LocaleDir = 'locales/' + this.config.language + '/';

            BetaEasy.Tools.InvokeButton.init();
            if (this.getPageYOffset() < 600) {
                document.getElementById('BetaEasyInvokeButton').style.display = 'none';
                return false;
            }
            BetaEasy.Tools.ContributionForm.init();
            BetaEasy.Tools.Page.addStyle(this.cssPopupStyles);

            if (this.config.styleType == "old") {
                //this.config.buttonHeight = 155
                this.config.buttonImageActive = 'newbtn-2.png';
                this.config.buttonImageHover = 'newbtn-2.png';
                this.cssButtonStyles = "#BetaEasyInvokeButton a {color:white; font-size:smaller; font-weight:bold; text-align:center; text-decoration:none; width: {$buttonWidth}px; height: {$buttonHeight}px; background: url({$cdnurl}css/themes/badges/{$feedButtonActive}) -35px 0px no-repeat; z-index: 1000000;}\r\n* #BetaEasyInvokeButton a:hover {width: {$buttonWidth}px; height: {$buttonHeight}px; background: url({$cdnurl}css/themes/badges/{$feedButtonHover}) -35px -137px no-repeat; z-index: 1000000;}\r\n* html #BeataEasyInvokeButtonBody {/*\*/position: absolute; top: expression(({$buttonTop} + (superMegaHelper = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)) + 'px'); right: expression(({$buttonRight} + (superMegaHelper2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft)) + 'px');/**/ }\r\n#BetaEasyInvokeButton > #BeataEasyInvokeButtonBody {position: fixed; top: {$buttonTop}px; right: {$buttonRight}px;}";
            } else {
                this.config.buttonImageHover = this.config.buttonImageActive;
                if (this.config.buttonAlign == 'right') {
                    this.cssButtonStyles = "#BetaEasyInvokeButton a {color:white; font-size:smaller; font-weight:bold; text-align:center; text-decoration:none; width: {$buttonWidth}px; height: {$buttonHeight}px; background: url({$cdnurl}css/themes/badges/{$feedButtonActive}) -35px 0px no-repeat; z-index: 1000000;}\r\n* #BetaEasyInvokeButton a:hover {width: {$buttonWidth}px; height: {$buttonHeight}px; background: url({$cdnurl}css/themes/badges/{$feedButtonActive}) -35px -137px no-repeat; z-index: 1000000;}\r\n* html #BeataEasyInvokeButtonBody {/*\*/position: absolute; top: expression(({$buttonTop} + (superMegaHelper = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)) + 'px'); right: expression(({$buttonRight} + (superMegaHelper2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft)) + 'px');/**/ }\r\n#BetaEasyInvokeButton > #BeataEasyInvokeButtonBody {position: fixed; top: {$buttonTop}px; right: {$buttonRight}px;}";
                } else {
                    this.cssButtonStyles = "#BetaEasyInvokeButton a {color:white; font-size:smaller; font-weight:bold; text-align:center; text-decoration:none; width: {$buttonWidth}px; height: {$buttonHeight}px; background: url({$cdnurl}css/themes/badges/{$feedButtonActive}) -0px 0px no-repeat; z-index: 1000000;}\r\n* #BetaEasyInvokeButton a:hover {width: {$buttonWidth}px; height: {$buttonHeight}px; background: url({$cdnurl}css/themes/badges/{$feedButtonActive}) 0px -137px no-repeat; z-index: 1000000;}\r\n* html #BeataEasyInvokeButtonBody {/*\*/position: absolute; top: expression(({$buttonTop} + (superMegaHelper = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop)) + 'px'); right: expression(({$buttonRight} + (superMegaHelper2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft)) + 'px');/**/ }\r\n#BetaEasyInvokeButton > #BeataEasyInvokeButtonBody {position: fixed; top: {$buttonTop}px; left: {$buttonRight}px;}";
                }

            }
            
            BetaEasy.Tools.Page.addStyle(tplCompile(this.cssButtonStyles, {
                buttonWidth: 35,
                buttonHeight: this.config.buttonHeight,
                buttonRight: 0,
                feedButtonActive:this.config.buttonImageActive,
                feedButtonHover:this.config.buttonImageHover,
                buttonTop: Math.round(this.getPageYOffset()/2-80/2),
                bgcolor: this.config.buttonBackgroundColor,
                bghovercolor: this.config.buttonMouseHoverBackgroundColor,
                cdnurl: this.getCdnUri()
            }));

        },

        getCdnUri: function() {
            return 'http://cdn.betaeasy.com/';
        },

        getPageYOffset: function() {
            
            var bodyHeight = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body.clientHeight;
            var browserHeight = window.innerHeight;

            if (browserHeight > bodyHeight) {
                return browserHeight;
            } else {
                return bodyHeight;
            }
            
        },

        getPageWidth: function() {
            return window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body.clientWidth;
        }
    }

    BetaEasy.Tools = {};

    BetaEasy.Tools.PageCover = {
        init: function() {
            this.render();
            return this;
        },

        show: function() {
            document.getElementById('BetaEasyPageCover').style.display = 'block';
        },
        
        hide: function() {
            document.getElementById('BetaEasyPageCover').style.display = 'none';
        },
        
        render: function() {
            if(!document.getElementById('BetaEasyPageCover')) {
                var pageCover = document.createElement('div');
                pageCover.innerHTML = '<div id="BetaEasyPageCover" onclick="BetaEasy.Tools.ContributionForm.hide(); return false;" style="display:none; width: 100%;"></div>';
                
                // calculates max page or document height
                var D = document;
                var browserHeight =  Math.max(
                    Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
                    Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
                    Math.max(D.body.clientHeight, D.documentElement.clientHeight)
                );
                
                pageCover.firstChild.style.height = browserHeight +'px';
                //pageCover.firstChild.style.height = (document.all ? document.body.offsetHeight : document.documentElement.scrollHeight) + 'px';
                
                if(document.all) {
                    pageCover.firstChild.style.width = (document.documentElement.clientWidth)+ 'px';
                }

                document.body.appendChild(pageCover.firstChild);
            }
            return false;
        }
    }

    BetaEasy.Tools.ContributionForm = {

        contributionFormTemplate: '<iframe src="' + BetaEasy.getCdnUri() + 'locales/{$language}/contributionForm.html?{$betaId}&url=' + document.location.href + '" scrolling="no" frameborder="0" allowtransparency="true" width="{$popupWidth}px" height="{$popupHeight}" onLoad="" style="height: {$popupHeight}px; width: {$popupWidth}px; "></iframe>',
        contributionFormErrorTemplate: '<div id="no_project" style="-moz-box-shadow:0 0 20px #CCCCCC; -webkit-box-shadow:0 0 20px #CCCCCC; -moz-border-radius: 5px; -webkit-border-radius: 5px; background-color:white; width:470px; height:100px; text-align:center;"><h2 style="padding-top: 35px; color:#595959; margin-right: 20px;">Your screen resolution to low</h2></div>',
        
        init: function() {

            this.preload();
            return this;
        },

        getIframeContent: function() {

        },

        render: function() {
            
            if(!document.getElementById('BetaEasyContributionForm')) {
                document.write('<div id="BetaEasyContributionForm"></div>');
            }
            
        },

        preload: function() {
            this.render();
        },

        show: function() {
            //TODO: Focus on input on contribution form when no any feedbacks
            BetaEasy.Tools.PageCover.init().show();
            
            var popup = document.getElementById('BetaEasyContributionForm');
       

            popup.style.display = 'block';
            
            popup.style.top = 
                  (document.all ? (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) : window.pageYOffset) +
                  Math.round(BetaEasy.getPageYOffset() / 2) - BetaEasy.config.popupHeight / 2
                + 'px';
            popup.style.left = Math.round(BetaEasy.getPageWidth() / 2 - BetaEasy.config.popupWidth / 2) + 'px';

            popup.innerHTML = tplCompile(this.contributionFormTemplate, BetaEasy.config);
            popup.innerHTML += '<div style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'' + BetaEasy.getCdnUri() +
                'images/close.png\'); display: inline-block; width:36px; height:36px; position:absolute; cursor:pointer; top:0px; right:0px" id="close" onclick="BetaEasy.Tools.ContributionForm.hide(); return false;">' +
                '<img src="' + BetaEasy.getCdnUri() + 'images/close.png" width="36px" height="36px" style="border:none; cursor:pointer; filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);" alt="close" />' +
                '</div>';

            return false;
        },

        hide: function() {
            document.getElementById('BetaEasyContributionForm').style.display = 'none';
            BetaEasy.Tools.PageCover.hide();
        }
    }

    BetaEasy.Tools.InvokeButton = {
        init: function() {
            this.render();
        },

        render: function() {
            document.write('<div id="BetaEasyInvokeButton"><a href="' + window.location + '#" id="BeataEasyInvokeButtonBody"><br /></a></div>');
            document.getElementById('BetaEasyInvokeButton').onclick = this.click;
        },

        click: function() {
            BetaEasy.Tools.ContributionForm.show();
            return false;
        }
    }

    BetaEasy.Tools.Page = {
        addStyle: function(css) {
            document.write('<style type="text/css">' + css + '</style>');
        },

        addScript: function() {
            
        }


    }

    
}