drop table if exists prescription;
drop table if exists doctor;
drop table if exists drug;
drop table if exists patient;
drop table if exists insuranceco;
drop table if exists pharmacist;
drop table if exists administrator;
drop table if exists securitytoken;
drop table if exists address;

create table address
(ID int(6) NOT NULL,  
 StreetNumberName VARCHAR(50), City VARCHAR(20), StateAbbreviation VARCHAR(2), PostalCode int(5),
 PRIMARY KEY USING BTREE(ID));

create table securitytoken
(ID int(6) NOT NULL,  
 Username VARCHAR(30), Hash VARCHAR(30), Role VARCHAR(25),
 PRIMARY KEY USING BTREE(ID));

 create table administrator
(ID int(6) NOT NULL,  
 FirstName VARCHAR(25), LastSurname VARCHAR(25), securitytokenID int(6),
 PRIMARY KEY USING BTREE(ID),
 FOREIGN KEY (securitytokenID) REFERENCES securitytoken(ID));
 
create table doctor
(ID int(6) NOT NULL, FirstName VARCHAR(25), LastSurname VARCHAR(25),
 securitytokenID int(6), addressID int(6), TelephoneNumber VARCHAR(10), Email VARCHAR(30),
 Specialization VARCHAR(25),
 PRIMARY KEY USING BTREE(ID),
 FOREIGN KEY (securitytokenID) REFERENCES securitytoken(ID),
 FOREIGN KEY (addressID) REFERENCES address(ID));
 
 create table drug
(ID int(6) NOT NULL, Name VARCHAR(25), CompanyName VARCHAR(25), addressID int(6),
 CompanyTelephoneNumber VARCHAR(10), CompanyContact VARCHAR(50), SideEffects VARCHAR(200),
 PRIMARY KEY USING BTREE(ID),
 FOREIGN KEY (addressID) REFERENCES address(ID));
 
 create table insuranceco
(ID int(6) NOT NULL, Name VARCHAR(25), addressID int(6), TelephoneNumber VARCHAR(10), Email VARCHAR(30),
 Contact VARCHAR(50), ContactPhone VARCHAR(10),
 PRIMARY KEY USING BTREE(ID),
 FOREIGN KEY (addressID) REFERENCES address(ID));
 
 create table patient
(ID int(6) NOT NULL, FirstName VARCHAR(25), LastSurname VARCHAR(25),
 Birthdate datetime, Gender VARCHAR(1), addressID int(6), insurancecoID int(6),
 TelephoneNumber VARCHAR(10), Email VARCHAR(30), Contact VARCHAR(50), ContactPhone VARCHAR(10),
 PRIMARY KEY USING BTREE(ID),
 FOREIGN KEY (addressID) REFERENCES address(ID),
 FOREIGN KEY (insurancecoID) REFERENCES insuranceco(ID));
 
 create table pharmacist
(ID int(6) NOT NULL, FirstName VARCHAR(25), LastSurname VARCHAR(25),
 securitytokenID int(6), addressID int(6), TelephoneNumber VARCHAR(10), Email VARCHAR(30),
 FirstLicensed datetime, StatesLicensed VARCHAR(50),
 PRIMARY KEY USING BTREE(ID),
 FOREIGN KEY (securitytokenID) REFERENCES securitytoken(ID),
 FOREIGN KEY (addressID) REFERENCES address(ID));
 
 create table prescription
(ID int(6) NOT NULL, patientID int(6), drugID int(6),
 Dosage VARCHAR(25), OrderDate datetime, Amount int(6), RefillAmount int(6),
 OrderFulfillDate datetime, PatientPickupDate datetime, doctorID int(6),  pharmacistID int(6),
 PRIMARY KEY USING BTREE(ID),
 FOREIGN KEY (patientID) REFERENCES patient(ID),
 FOREIGN KEY (drugID) REFERENCES drug(ID),
 FOREIGN KEY (doctorID) REFERENCES doctor(ID),
 FOREIGN KEY (pharmacistID) REFERENCES pharmacist(ID));
 
INSERT INTO `address` (`ID`,`StreetNumberName`,`City`,`StateAbbreviation`,`PostalCode`) VALUES (1,'123 Venture Way','Someplace','CA',12345);
INSERT INTO `address` (`ID`,`StreetNumberName`,`City`,`StateAbbreviation`,`PostalCode`) VALUES (2,'323 Some Street','Nowhere','GA',30033);
INSERT INTO `address` (`ID`,`StreetNumberName`,`City`,`StateAbbreviation`,`PostalCode`) VALUES (3,'666 Evil Drive','Inferno','FL',66666);
INSERT INTO `address` (`ID`,`StreetNumberName`,`City`,`StateAbbreviation`,`PostalCode`) VALUES (4,'1337 Street','Pwned','TN',56424);
INSERT INTO `address` (`ID`,`StreetNumberName`,`City`,`StateAbbreviation`,`PostalCode`) VALUES (5,'555 Main Street','Atlanta','GA',30302);
INSERT INTO `address` (`ID`,`StreetNumberName`,`City`,`StateAbbreviation`,`PostalCode`) VALUES (6,'234 Sunny Lane','Atlanta','GA',30303);

INSERT INTO `securitytoken` (`ID`,`Username`,`Hash`,`Role`) VALUES (1,'eguevara','d9ed67d2bc6aa07d95f5677c472','Doctor');
INSERT INTO `securitytoken` (`ID`,`Username`,`Hash`,`Role`) VALUES (2,'qball','ba461fa13a5dd6325dffa17aaa7b2190','Doctor');
INSERT INTO `securitytoken` (`ID`,`Username`,`Hash`,`Role`) VALUES (3,'pentmann','0c0975ec33c93d329c3bdcbd4c8dab8','Doctor');
INSERT INTO `securitytoken` (`ID`,`Username`,`Hash`,`Role`) VALUES (4,'mgoldman','4b20993989074973b7daa05636cf55','Pharmacist');
INSERT INTO `securitytoken` (`ID`,`Username`,`Hash`,`Role`) VALUES (5,'sguy','968fbfb41be3d7afacf2a2cb29916','Pharmacist');
INSERT INTO `securitytoken` (`ID`,`Username`,`Hash`,`Role`) VALUES (6,'jadmin','78b2c5fcf99ed1f47e35a86e6c4f128','Administrator');
INSERT INTO `securitytoken` (`ID`,`Username`,`Hash`,`Role`) VALUES (7,'bdavis','74435a44b0b5aa0d2988ae220fa4f61','Administrator');
INSERT INTO `securitytoken` (`ID`,`Username`,`Hash`,`Role`) VALUES (8,'cmiller','d0c53e1fcaa78174a5adff5b57294d','Doctor');
INSERT INTO `securitytoken` (`ID`,`Username`,`Hash`,`Role`) VALUES (9,'kbechtel','9f8f37a465765f4b35d0ef70ff71f8a4','Pharmacist');
INSERT INTO `securitytoken` (`ID`,`Username`,`Hash`,`Role`) VALUES (10,'kfehd','daa14d3358a8cbbc0cfa1996e258d4f','Administrator');

INSERT INTO `administrator` (`ID`,`FirstName`,`LastSurname`,`securitytokenID`) VALUES (1,'Johnny','Admin',6);
INSERT INTO `administrator` (`ID`,`FirstName`,`LastSurname`,`securitytokenID`) VALUES (2,'Bryan','Davis',7);
INSERT INTO `administrator` (`ID`,`FirstName`,`LastSurname`,`securitytokenID`) VALUES (3,'Karl','Fehd',10);

INSERT INTO `doctor` (`ID`,`FirstName`,`LastSurname`,`securitytokenID`,`addressID`,`TelephoneNumber`,`Email`,`Specialization`) VALUES (1,'Ernesto','Guevara',1,2,'4041234567','eguevara@webmd.com','Internal Medicine');
INSERT INTO `doctor` (`ID`,`FirstName`,`LastSurname`,`securitytokenID`,`addressID`,`TelephoneNumber`,`Email`,`Specialization`) VALUES (2,'Quentin','Ball',2,4,'6781234567','Qball@icup.com','Family Medicine');
INSERT INTO `doctor` (`ID`,`FirstName`,`LastSurname`,`securitytokenID`,`addressID`,`TelephoneNumber`,`Email`,`Specialization`) VALUES (3,'Paul','Entmann',3,5,'7701234567','paul.entmann@hospital.org','Anasthesiology');
INSERT INTO `doctor` (`ID`,`FirstName`,`LastSurname`,`securitytokenID`,`addressID`,`TelephoneNumber`,`Email`,`Specialization`) VALUES (4,'Christopher','Miller',8,6,'4042325656','cmiller@spsu.edu','Psychiatry');

INSERT INTO `drug` (`ID`,`Name`,`CompanyName`,`addressID`,`CompanyTelephoneNumber`,`CompanyContact`,`SideEffects`) VALUES (1,'WeightWeight Gain 4000','Beefcake Industries',4,'4567891425','Matt Stone','Getting Totally Ripped');
INSERT INTO `drug` (`ID`,`Name`,`CompanyName`,`addressID`,`CompanyTelephoneNumber`,`CompanyContact`,`SideEffects`) VALUES (2,'Ritalin','Behaivior Pharm',2,'5551234678','Trey Parker','Calming Down');
INSERT INTO `drug` (`ID`,`Name`,`CompanyName`,`addressID`,`CompanyTelephoneNumber`,`CompanyContact`,`SideEffects`) VALUES (3,'Ritalout','Behaivior Pharm',2,'5551234678','Trey Parker','Waking Up');
 
INSERT INTO `insuranceco` (`ID`,`Name`,`addressID`,`TelephoneNumber`,`Email`,`Contact`,`ContactPhone`) VALUES (1,'Medicare',2,'2025658989','Support@Medicare.gov','Uncle Sam','6521234848');
INSERT INTO `insuranceco` (`ID`,`Name`,`addressID`,`TelephoneNumber`,`Email`,`Contact`,`ContactPhone`) VALUES (2,'BCBS',5,'2225556666','CustomerCare@bcbs.com','Noh Wan','4448883333');

INSERT INTO `patient` (`ID`,`FirstName`,`LastSurname`,`Birthdate`,`Gender`,`addressID`,`insurancecoID`,`TelephoneNumber`,`Email`,`Contact`,`ContactPhone`) VALUES (1,'Rusty','Venture','1970-12-25 00:00:00','M',1,2,'1234567890','rustyrocks@hotmail.com','Jonas Venture','4567891230');
INSERT INTO `patient` (`ID`,`FirstName`,`LastSurname`,`Birthdate`,`Gender`,`addressID`,`insurancecoID`,`TelephoneNumber`,`Email`,`Contact`,`ContactPhone`) VALUES (2,'Broc','Samson','1965-08-15 00:00:00','M',1,1,'7778889999','bsamson@osi.gov',null,null);
INSERT INTO `patient` (`ID`,`FirstName`,`LastSurname`,`Birthdate`,`Gender`,`addressID`,`insurancecoID`,`TelephoneNumber`,`Email`,`Contact`,`ContactPhone`) VALUES (3,'Roy','Brisby','1954-01-12 00:00:00','M',3,2,'6665554444','busybee@isp.com','Mandalay','7178289696');
INSERT INTO `patient` (`ID`,`FirstName`,`LastSurname`,`Birthdate`,`Gender`,`addressID`,`insurancecoID`,`TelephoneNumber`,`Email`,`Contact`,`ContactPhone`) VALUES (4,'Triana','Orpheus','1994-10-31 00:00:00','F',1,1,'1112223333',null,'Byron Orpheus','1314647979');
INSERT INTO `patient` (`ID`,`FirstName`,`LastSurname`,`Birthdate`,`Gender`,`addressID`,`insurancecoID`,`TelephoneNumber`,`Email`,`Contact`,`ContactPhone`) VALUES (5,'Phantom','Limb','1966-06-06 00:00:00','M',4,2,'3336669999','jsmith@aol.com','Sheila','4546564646');
INSERT INTO `patient` (`ID`,`FirstName`,`LastSurname`,`Birthdate`,`Gender`,`addressID`,`insurancecoID`,`TelephoneNumber`,`Email`,`Contact`,`ContactPhone`) VALUES (6,'Sally','Impossible','1981-05-11 00:00:00','F',5,2,'2225558888','nowyouseeme@impossiblelabs.org','Richard Impossible','8587479393');

INSERT INTO `pharmacist` (`ID`,`FirstName`,`LastSurname`,`securitytokenID`,`addressID`,`TelephoneNumber`,`Email`,`FirstLicensed`,`StatesLicensed`) VALUES (1,'Mort','Goldman',4,6,'6465852525','mgoldman@pharm.com','1985-05-10 00:00:00','NY');
INSERT INTO `pharmacist` (`ID`,`FirstName`,`LastSurname`,`securitytokenID`,`addressID`,`TelephoneNumber`,`Email`,`FirstLicensed`,`StatesLicensed`) VALUES (2,'Sum','Guy',5,3,'3336664444','sumemail@address.com','1978-08-20 00:00:00','GA');
INSERT INTO `pharmacist` (`ID`,`FirstName`,`LastSurname`,`securitytokenID`,`addressID`,`TelephoneNumber`,`Email`,`FirstLicensed`,`StatesLicensed`) VALUES (3,'Kyle','Bechtel',9,5,'6782328888','kbechtel@spsu.edu','2013-04-01 00:00:00','GA');

INSERT INTO `prescription` (`ID`,`patientID`,`drugID`,`Dosage`,`OrderDate`,`Amount`,`RefillAmount`,`OrderFulfillDate`,`PatientPickupDate`,`doctorID`,`pharmacistID`) VALUES (1,1,1,'300mg','2014-01-21 00:00:00',90,2,'2014-01-21 00:00:00','2014-01-22 00:00:00',1,1);
INSERT INTO `prescription` (`ID`,`patientID`,`drugID`,`Dosage`,`OrderDate`,`Amount`,`RefillAmount`,`OrderFulfillDate`,`PatientPickupDate`,`doctorID`,`pharmacistID`) VALUES (2,3,2,'10mg','2014-02-04 00:00:00',30,4,'2014-02-07 00:00:00','2014-02-07 00:00:00',2,2);
INSERT INTO `prescription` (`ID`,`patientID`,`drugID`,`Dosage`,`OrderDate`,`Amount`,`RefillAmount`,`OrderFulfillDate`,`PatientPickupDate`,`doctorID`,`pharmacistID`) VALUES (3,4,2,'10mg','2014-03-03 00:00:00',30,3,'2014-03-03 00:00:00','2014-03-07 00:00:00',2,1);
INSERT INTO `prescription` (`ID`,`patientID`,`drugID`,`Dosage`,`OrderDate`,`Amount`,`RefillAmount`,`OrderFulfillDate`,`PatientPickupDate`,`doctorID`,`pharmacistID`) VALUES (4,4,3,'5mg','2014-03-08 00:00:00',30,3,'2014-03-08 00:00:00','2014-03-08 00:00:00',3,2);
INSERT INTO `prescription` (`ID`,`patientID`,`drugID`,`Dosage`,`OrderDate`,`Amount`,`RefillAmount`,`OrderFulfillDate`,`PatientPickupDate`,`doctorID`,`pharmacistID`) VALUES (5,5,2,'500mg','2014-02-14 00:00:00',90,4,'2014-02-14 00:00:00','2014-02-14 00:00:00',4,3);